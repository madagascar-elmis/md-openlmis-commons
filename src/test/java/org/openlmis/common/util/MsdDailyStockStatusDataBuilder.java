/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */


package org.openlmis.common.util;

import java.util.Set;
import java.util.UUID;
import org.javers.common.collections.Sets;
import org.openlmis.common.domain.MsdDailyStockStatus;
import org.openlmis.common.domain.MsdDailyStockStatusValue;

public class MsdDailyStockStatusDataBuilder {

  private static int instanceNumber = 0;

  private UUID id;

  private String ilNumber;

  private Set<MsdDailyStockStatusValue> values;

  /**
   * Builds instance of {@link MsdDailyStockStatusDataBuilder} with sample data.
   */
  public MsdDailyStockStatusDataBuilder() {
    instanceNumber++;
    id = UUID.randomUUID();
    ilNumber = "il-" + instanceNumber;
    values = Sets.asSet(
        new MsdDailyStockStatusValueDataBuilder().build(),
        new MsdDailyStockStatusValueDataBuilder().build()
    );
  }

  /**
   * Builds instance of {@link MsdDailyStockStatus}.
   */
  public MsdDailyStockStatus build() {
    MsdDailyStockStatus stockStatus = new MsdDailyStockStatus();
    stockStatus.setId(id);
    stockStatus.setIlNumber(ilNumber);
    stockStatus.setValues(values);
    return stockStatus;
  }

  /**
   * Builds instance of {@link MsdDailyStockStatus} without id.
   */
  public MsdDailyStockStatusDataBuilder withoutId() {
    this.id = null;
    this.values.forEach(value -> value.setId(null));
    return this;
  }

}
