/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.domain;

import java.util.UUID;
import org.openlmis.common.dto.StockNotificationLineItemDto;

public class StockNotificationLineItemDataBuilder extends StockLineItemBaseTest {

  /**
   * Returns instance of {@link StockNotificationLineItemDataBuilder} with sample data.
   */
  public StockNotificationLineItemDataBuilder() {

    id = UUID.fromString("15498136-8972-4ade-9f14-602bf199652b");
    itemCode = "Item-code-123";
    itemDescription = "item-Description-123";
    uom = "1Tu";
    quantity = 1500;
    quantityShipped = 1246;
    quantityOrdered = 8102;
    missingItemStatus = "OUT-OF-STOCK";
    dueDate = "2018-10-25";

  }

  /**
   * Creates new instance of {@link org.openlmis.common.domain.StockNotificationLineItem}
   * with provided data without id.
   */
  public StockNotificationLineItem buildAsNew() {

    return new StockNotificationLineItem(itemCode, itemDescription, uom,
        quantity, quantityShipped, quantityOrdered, missingItemStatus, dueDate);
  }

  /**
   * Creates new instance of {@link StockNotificationLineItem} with provided data.
   */
  @Override
  public StockNotificationLineItem build() {
    StockNotificationLineItem lineItem = buildAsNew();
    lineItem.setId(id);

    return lineItem;
  }

  /**
   * Builds {@link StockNotificationLineItemDto} test data instance.
   *
   * @return StockNotificationLineItemDto
   */
  public StockNotificationLineItemDto buildAsDto() {

    StockNotificationLineItem lineItem = build();
    StockNotificationLineItemDto lineItemDto = new StockNotificationLineItemDto();
    lineItem.export(lineItemDto);
    return lineItemDto;

  }

}
