/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.domain;

import java.util.UUID;

public class StockNotificationDataBuilder extends StockNotificationBuilderBaseTest {

  /**
   * Creates new instance of {@link StockNotification}.
   */

  @Override
  public StockNotification build() {

    StockNotification stockNotification = new StockNotification(
        id, quoteNumber, customerId, customerName, hfrCode, elmisOrderNumber,
        notificationDate, processingDate, zone, comment, requisitionId
    );

    stockNotification.setId(id);
    lineItems.forEach(lineItem -> lineItem.setNotification(stockNotification));
    return stockNotification;
  }

  /**
   * Sets id.
   */
  public StockNotificationDataBuilder withId(UUID id) {
    this.id = id;
    return this;
  }

  @Override
  public StockNotificationDataBuilder withQuoteNumber(String quoteNumber) {
    this.quoteNumber = quoteNumber;
    return this;
  }

  public StockNotificationDataBuilder withCustomerName(String customerName) {
    this.customerName = customerName;
    return this;
  }

  public StockNotificationDataBuilder withHfrCode(String hfrCode) {
    this.hfrCode = hfrCode;
    return this;
  }

  public StockNotificationDataBuilder withCustomerId(String customerId) {
    this.customerId = customerId;
    return this;
  }

  public StockNotificationDataBuilder withElmisOrderNumber(String elmisOrderNumber) {
    this.elmisOrderNumber = elmisOrderNumber;
    return this;
  }

}
