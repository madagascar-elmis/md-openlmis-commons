/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.domain;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.util.UUID;
import org.junit.Test;
import org.openlmis.common.ToStringTestUtils;

public class StockNotificationLineItemTest {

  private UUID id = UUID.randomUUID();
  private String itemCode = "Item-code-123";
  private String itemDescription = "item-Description-123";
  private String uom = "1Tu";
  private Integer quantity = 1500;
  private Integer quantityOrdered = 8102;
  private String missingItemStatus = "OUT-OF-STOCK";
  private String dueDate = "2018-10-25";

  @Test
  public void shouldExportValues() {

    DummyStockNotificationLineItemDto exporter
        = new DummyStockNotificationLineItemDto();

    StockNotificationLineItem lineItem = createLineItem();
    lineItem.export(exporter);

    assertEquals(id, exporter.getId());

  }

  @Test
  public void shouldImplementToString() {
    StockNotificationLineItem lineItem = new StockNotificationLineItemDataBuilder().build();
    ToStringTestUtils.verify(StockNotificationLineItem.class, lineItem);
  }

  @Test
  public void shouldReturnTrueIfLineItemHasSomethingOrdered() {
    assertThat(createLineItem().getQuantityOrdered() > 0, is(true));
  }

  private StockNotificationLineItem createLineItem() {

    return new StockNotificationLineItemDataBuilder()
        .withId(id)
        .withItemCode(itemCode)
        .withItemDescription(itemDescription)
        .withUom(uom)
        .withQuantity(quantity)
        .withQuantityOrdered(quantityOrdered)
        .withMissingItemStatus(missingItemStatus)
        .withDueDate(dueDate)
        .build();
  }

}
