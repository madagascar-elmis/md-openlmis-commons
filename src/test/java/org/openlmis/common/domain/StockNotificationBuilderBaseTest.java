/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.domain;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;
import org.apache.commons.lang.RandomStringUtils;
import org.assertj.core.util.Lists;

public abstract class StockNotificationBuilderBaseTest {

  protected UUID id = UUID.randomUUID();
  protected String quoteNumber = RandomStringUtils.randomAlphanumeric(10);
  protected String customerId = RandomStringUtils.randomAlphanumeric(25);
  protected String customerName = RandomStringUtils.randomAlphanumeric(15);
  protected String hfrCode = RandomStringUtils.randomAlphanumeric(7);
  protected String elmisOrderNumber = RandomStringUtils.randomAlphanumeric(6);
  protected String notificationDate = LocalDate.now().toString();
  protected String processingDate = String.valueOf(LocalDate.now().minusDays(1));
  protected String zone = RandomStringUtils.randomAlphanumeric(2);
  protected String comment = "Some Comments";
  protected UUID requisitionId = UUID.randomUUID();
  protected List<StockNotificationLineItem> lineItems = Lists.newArrayList();

  public StockNotificationBuilderBaseTest withNotificationDate(String notificationDate) {
    this.notificationDate = notificationDate;
    return this;
  }

  public StockNotificationBuilderBaseTest withProcessingDate(String processingDate) {
    this.processingDate = processingDate;
    return this;
  }

  public StockNotificationBuilderBaseTest withZone(String zone) {
    this.zone = zone;
    return this;
  }

  public StockNotificationBuilderBaseTest withComment(String comment) {
    this.comment = comment;
    return this;
  }

  public StockNotificationBuilderBaseTest withRequisitionId(UUID requisitionId) {
    this.requisitionId = requisitionId;
    return this;
  }

  /**
   * Sets stock notification line items.
   */
  public StockNotificationBuilderBaseTest withLineItems(
      List<StockNotificationLineItem> notificationLineItems
  ) {
    this.lineItems = notificationLineItems;
    return this;
  }

  public abstract StockNotification build();

  public abstract StockNotificationDataBuilder withQuoteNumber(String quoteNumber);
}
