SELECT ft.facility,district,region,

CASE
    WHEN msdzone IS NULL THEN (SELECT gz.name from public.kafka_geographic_zones gz 
	 INNER JOIN public.kafka_geographic_levels gl ON gz.levelid=gl.id
	 WHERE gl.levelnumber=1 AND (gz.id=grandparentid OR gz.id=ft.parentid))
    ELSE msdzone     
END as msdzone,

ft.reason,category,program,period,schedule,
(SELECT DATE_PART('year', rdate::DATE)) AS year

FROM
(SELECT rr.name as reason,f.name as facility,rrc.name as category,p.name as program,pp.name as period,
r.createddate as rdate,psch.name as schedule,districts.name as district,

CASE 
   WHEN regions.name IS NULL THEN (SELECT gz.name from public.kafka_geographic_zones gz 
        INNER JOIN public.kafka_geographic_levels gl ON gz.levelid=gl.id
        WHERE gl.levelnumber=2 AND gz.id=gzones.parentid)
   ELSE regions.name
END as region,

msdzones.name as msdzone,
gzones.parentid, 
(SELECT z.parentid FROM public.kafka_geographic_zones z WHERE z.id = gzones.parentid) as grandparentid


FROM public.kafka_rejections r 
INNER JOIN public.kafka_rejection_reasons rr ON r.rejectionreasonid=rr.id 
INNER JOIN public.kafka_status_changes sc ON r.statuschangeid=sc.id::TEXT
INNER JOIN public.kafka_requisitions req ON sc.requisitionid=req.id
INNER JOIN public.kafka_facilities f ON f.id=req.facilityid
INNER JOIN public.kafka_rejection_reason_categories rrc ON rr.rejectionreasoncategoryid=rrc.id
INNER JOIN public.kafka_programs p ON req.programid=p.id
INNER JOIN public.kafka_processing_periods pp ON req.processingperiodid=pp.id
INNER JOIN public.kafka_processing_schedules psch ON pp.processingscheduleid=psch.id
INNER JOIN public.kafka_geographic_zones gzones ON f.geographiczoneid=gzones.id

LEFT JOIN 
(SELECT gz.id,gz.name from public.kafka_geographic_zones gz 
INNER JOIN public.kafka_geographic_levels gl ON gz.levelid=gl.id
WHERE gl.levelnumber=3) as districts ON f.geographiczoneid=districts.id

LEFT JOIN 
(SELECT gz.id,gz.name,gl.levelnumber from public.kafka_geographic_zones gz 
INNER JOIN public.kafka_geographic_levels gl ON gz.levelid=gl.id
WHERE gl.levelnumber=2) as regions ON f.geographiczoneid=regions.id

LEFT JOIN 
(SELECT gz.id,gz.name,gl.levelnumber from public.kafka_geographic_zones gz 
INNER JOIN public.kafka_geographic_levels gl ON gz.levelid=gl.id
WHERE gl.levelnumber=1) as msdzones ON f.geographiczoneid=msdzones.id
) as ft;
