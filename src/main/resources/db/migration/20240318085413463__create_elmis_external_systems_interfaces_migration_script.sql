DROP TABLE IF EXISTS common.interface_dataset;
DROP TABLE IF EXISTS common.interface_apps;

CREATE TABLE common.interface_apps
(
    id     SERIAL PRIMARY KEY,
    code   TEXT NOT NULL,
    name   TEXT NOT NULL,
    active BOOLEAN DEFAULT TRUE
);

CREATE TABLE common.interface_dataset
(
    id           SERIAL PRIMARY KEY,
    interfaceid  INTEGER NOT NULL,
    datasetname  TEXT NOT NULL,
    datasetid    TEXT  NOT NULL,
    CONSTRAINT interface_dataset_interfaceid_fkey FOREIGN KEY (interfaceid) REFERENCES common.interface_apps (id)
);