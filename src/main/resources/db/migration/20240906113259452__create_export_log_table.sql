-- WHEN COMMITTING OR REVIEWING THIS FILE: Make sure that the timestamp in the file name (that serves as a version) is the latest timestamp, and that no new migration have been added in the meanwhile.
-- Adding migrations out of order may cause this migration to never execute or behave in an unexpected way.
-- Migrations should NOT BE EDITED. Add a new migration to apply changes.
-- Create table to store import responses
CREATE TABLE export_log (
                                  id SERIAL PRIMARY KEY,
                                  httpstatus VARCHAR(10),
                                  httpstatuscode INT,
                                  status VARCHAR(10),
                                  message TEXT,
                                  responsetype VARCHAR(50),
                                  responsestatus VARCHAR(10),
                                  importoptions JSONB,
                                  description TEXT,
                                  imported INT,
                                  updated INT,
                                  ignored INT,
                                  deleted INT,
                                  conflicts JSONB,
                                  rejectedindexes JSONB,
                                  datasetcomplete BOOLEAN,
                                  createdat TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);


