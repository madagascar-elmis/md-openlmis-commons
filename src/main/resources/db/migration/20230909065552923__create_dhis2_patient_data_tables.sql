--
-- Name: data_responses; Type: TABLE; Schema: common; Owner: postgres; Tablespace:
--

CREATE TABLE data_responses
(
    id      UUID NOT NULL,
    page     integer,
    pageCount integer,
    total     integer,
    pageSize     integer,
    CONSTRAINT data_responses_pkey PRIMARY KEY (id)
);

--
-- Name: dhis_patients; Type: TABLE; Schema: common; Owner: postgres; Tablespace:
--

CREATE TABLE dhis_patients
(
    id      UUID NOT NULL,
    periodId     text NOT NULL,
    facilityCode   text NOT NULL,
    createdDate  timestamptz default now(),
    modifiedDate timestamptz default now(),
    responseId UUID,
    CONSTRAINT dhis_patients_pkey PRIMARY KEY (id),
    CONSTRAINT fkey_dhis_patients_data_responses FOREIGN KEY (responseId) REFERENCES data_responses (id)
);

--
-- Name: dhis_patient_line_items; Type: TABLE; Schema: common; Owner: postgres; Tablespace:
--

create table dhis_patient_line_items
(
    id UUID NOT NULL,
    patientId       UUID,
    productCode     TEXT NOT NULL,
    value           integer,
    lastUpdatedDate TEXT,
    description     TEXT,
    CONSTRAINT dhis_patient_line_items_pkey PRIMARY KEY (id),
    CONSTRAINT fkey_dhis_patients_line_item FOREIGN KEY (patientId) REFERENCES dhis_patients (id)

);