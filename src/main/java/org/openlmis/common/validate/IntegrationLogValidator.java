/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.validate;

import org.openlmis.common.dto.IntegrationLogDto;
import org.openlmis.common.i18n.ValidationMessageKeys;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

/**
 * A validator for {@link org.openlmis.common.dto.IntegrationLogDto} object.
 */
@Component
public class IntegrationLogValidator implements BaseValidator {

  /**
   * Checks if the given class definition is supported.
   *
   * @param clazz the {@link Class} that this
   *              {@link org.springframework.validation.Validator} is being asked if it can {@link
   *              #validate(Object, Errors) validate}
   *
   * @return true if {@code clazz} is equal to {@link IntegrationLogDto} class definition.
   *     Otherwise false.
   */
  @Override
  public boolean supports(Class<?> clazz) {
    return IntegrationLogDto.class.equals(clazz);
  }

  /**
   * Validates the {@code target} object, which must be an instance
   * of {@link IntegrationLogDto} class.
   *
   * @param target the object that is to be validated (never {@code null})
   * @param errors contextual state about the validation process (never {@code null})
   */
  @Override
  public void validate(Object target, Errors errors) {
    verifyArguments(target, errors, ValidationMessageKeys.ERROR_CONTEXTUAL_STATE_NULL);
    rejectIfEmptyOrWhitespace(errors, "destinationId",
        ValidationMessageKeys.ERROR_DESTINATION_ID_REQUIRED);
    rejectIfEmptyOrWhitespace(errors, "sourceTransactionId",
        ValidationMessageKeys.ERROR_SOURCE_TRANSACTION_ID_REQUIRED);
    rejectIfEmptyOrWhitespace(errors, "errorMessage",
        ValidationMessageKeys.ERROR_MESSAGE_REQUIRED);
    rejectIfEmptyOrWhitespace(errors, "integrationName",
        ValidationMessageKeys.ERROR_INTEGRATION_NAME_REQUIRED);
    rejectIfEmptyOrWhitespace(errors, "errorCode",
        ValidationMessageKeys.ERROR_CODE_REQUIRED);
  }
}