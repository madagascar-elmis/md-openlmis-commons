/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.service.supportdesk;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.commons.io.IOUtils;
import org.json.JSONObject;
import org.openlmis.common.dto.supportdesk.SdIssue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class RestSdService {

  private final Logger logger = LoggerFactory.getLogger(getClass());

  @Value("${jira.tz.url}")
  private String jiraUrl;

  @Value("${jira.tz.service-desk.id}")
  private String jiraServiceDeskId;

  @Value("${jira.tz.authorization.key}")
  private String jiraAuthorizationKey;

  @Value("${jira.tz.service-desk.request-type-id}")
  private String requestTypeId;

  /**
   * Retrieve articles by search keyword.
   *
   * @param query String of searched key.
   *
   * @return searched articles.
   */
  public String getArticlesBySearchKeyword(String query) {

    try {

      GetMethod method = new GetMethod(jiraUrl + "/rest/servicedeskapi/"
          + "servicedesk/" + jiraServiceDeskId + "/knowledgebase/article?query="
          + query.replace(" ", "%"));

      method.setRequestHeader("Accept", "application/json");
      method.setRequestHeader("Authorization", "Basic " + jiraAuthorizationKey);
      method.setRequestHeader("X-ExperimentalApi", "opt-in");
      method.getParams().setParameter(HttpMethodParams.RETRY_HANDLER,
          new DefaultHttpMethodRetryHandler(3, false));

      HttpClient client = new HttpClient();

      client.executeMethod(method);
      byte[] responseBody = method.getResponseBody();

      logger.debug("response = {}", responseBody);

      return new String(responseBody);
    } catch (IOException e) {
      logger.debug("error = {}", e.getMessage());
      return null;
    }

  }

  /**
   * report an issue.
   *
   * @param sdIssue String of content to be reported.
   *
   * @return content.
   */
  public String createIssue(SdIssue sdIssue) {

    String bodyData = new JSONObject()
        .put("raiseOnBehalfOf", sdIssue.getRaiseOnBehalfOf())
        .put("serviceDeskId", jiraServiceDeskId)
        .put("requestTypeId", requestTypeId)
        .put("requestFieldValues", new JSONObject()
            .put("summary", sdIssue.getSummary())
            .put("description", sdIssue.getDescription())
            .put("type", sdIssue.getIssueType())
            .put("impact", sdIssue.getImpact())
            .put("priority", sdIssue.getPriority())).toString();

    PostMethod method = new PostMethod(jiraUrl + "/rest/servicedeskapi/request");

    method.setRequestHeader("Accept", "application/json");
    method.setRequestHeader("Content-Type", "application/json");
    method.setRequestHeader("Authorization", "Basic " + jiraAuthorizationKey);
    method.setRequestHeader("X-ExperimentalApi", "opt-in");
    method.setRequestBody(bodyData);
    method.getParams().setParameter(HttpMethodParams.RETRY_HANDLER,
        new DefaultHttpMethodRetryHandler(3, false));

    try {
      HttpClient client = new HttpClient();
      client.executeMethod(method);
      InputStream responseBodyAsStream = method.getResponseBodyAsStream();
      return IOUtils.toString(responseBodyAsStream, StandardCharsets.UTF_8);
    } catch (IOException e) {
      logger.debug("error ={}", e.getMessage());
    }
    return null;
  }

}
