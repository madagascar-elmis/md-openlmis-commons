/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.service.integration;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.time.LocalDateTime;
import java.util.List;
import lombok.AllArgsConstructor;
import org.openlmis.common.domain.integration.ExportLog;
import org.openlmis.common.dto.integration.ElmisInterfaceDto;
import org.openlmis.common.repository.integration.ExportLogRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * The ExportLogService class .
 */

@Service
@AllArgsConstructor
public class ExportLogService {

  private final ExportLogRepository exportLogRepository;

  private final ObjectMapper objectMapper;

  private static final Logger logger = LoggerFactory.getLogger(ExportLogService.class);


  /**
   * The saveResponseAsExportLog method .
   */

  public ExportLog saveResponseAsExportLog(String jsonResponse,  ElmisInterfaceDto data) {
    JsonNode rootNode = null;
    try {
      rootNode = objectMapper.readTree(jsonResponse);
    } catch (JsonProcessingException e) {
      logger.error("Can't convert string to extraData", e);
      return null;
    }

    ExportLog exportLog = new ExportLog();
    exportLog.setHttpStatus(rootNode.path("httpStatus").asText());
    exportLog.setHttpStatusCode(rootNode.path("httpStatusCode").asInt());
    exportLog.setStatus(rootNode.path("status").asText());
    exportLog.setMessage(rootNode.path("message").asText());

    JsonNode responseNode = rootNode.path("response");
    exportLog.setResponseType(responseNode.path("responseType").asText());
    exportLog.setResponseStatus(responseNode.path("status").asText());
    exportLog.setImportOptions(responseNode.path("importOptions").toString());
    exportLog.setDescription(responseNode.path("description").asText());

    JsonNode importCountNode = responseNode.path("importCount");
    exportLog.setImported(importCountNode.path("imported").asInt());
    exportLog.setUpdated(importCountNode.path("updated").asInt());
    exportLog.setIgnored(importCountNode.path("ignored").asInt());
    exportLog.setDeleted(importCountNode.path("deleted").asInt());

    exportLog.setConflicts(responseNode.path("conflicts").toString());
    exportLog.setRejectedIndexes(responseNode.path("rejectedIndexes").toString());
    exportLog.setDataSetComplete(responseNode.path("dataSetComplete").asBoolean());
    exportLog.setCreatedAt(LocalDateTime.now());
    return exportLogRepository.save(exportLog);
  }

  public List<ExportLog> getAllExportLogs() {
    return exportLogRepository.findAll();
  }

  public ExportLog getExportLogById(Long id) {
    return exportLogRepository.findById(id).orElse(null);
  }

  public void deleteExportLogById(Long id) {
    exportLogRepository.deleteById(id);
  }
}
