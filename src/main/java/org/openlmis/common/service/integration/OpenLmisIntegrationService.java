/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.service.integration;

import java.util.List;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import org.openlmis.common.domain.integration.IntegrationData;
import org.openlmis.common.dto.integration.ElmisInterfaceDataSetDto;
import org.openlmis.common.dto.integration.ElmisInterfaceDto;
import org.openlmis.common.repository.integration.IntegrationDataRepository;
import org.openlmis.common.repository.integration.InterfaceMapper;
import org.openlmis.common.util.Dhis2Client;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * OpenLmisIntegrationService Class.
 */
@Service
@AllArgsConstructor
public class OpenLmisIntegrationService {
  private final IntegrationDataRepository repository;
  private final Dhis2Client dhis2Client;

  public static final String URL = "LLIN_DHIS_URL";

  /**
     * Scheduled method to process consumption data.
     */
  @Scheduled(cron = "${openlmis.integration.cron}", zone = "${time.zoneId}")
  @Async
  public void processConsumptionData() {

    repository.refreshMaterializedView();

    String username = "admin";
    String password = "district";
    String url = "https://opendev.snis-sante.org/api/dataValueSets";

    List<String> periods = repository.findPeriodsFromStartDate("2024-01-01");


    ElmisInterfaceDto dto;
    for (String period : periods) {
      int page = 0;
      Pageable pageable;
      Page<IntegrationData> currentPage;
      dto = new ElmisInterfaceDto();
      do {
        pageable = PageRequest.of(page, 500);
        currentPage = repository.getPeriodConsumptionData(Integer.parseInt(period), pageable);
        if (currentPage.getContent().isEmpty()) {
          continue;
        }
        List<ElmisInterfaceDataSetDto> dataValues = currentPage.getContent().stream()
                          .map(InterfaceMapper::toDto)
                          .collect(Collectors.toList());
        dto.setDataValues(dataValues);
        dhis2Client.pushDataToDhis(username, password, url, dto);
        page++;
      } while (currentPage.hasNext());
    }

  }
}
