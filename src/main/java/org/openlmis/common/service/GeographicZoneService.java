/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.service;

import java.util.List;
import org.openlmis.common.dto.GeoFacilityIndicatorDto;
import org.openlmis.common.dto.GeoZoneReportingRateDto;
import org.openlmis.common.repository.GeoIndicatorResultRepository;
import org.openlmis.common.repository.GeoReportingRateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GeographicZoneService {

  private final GeoReportingRateRepository repository;

  private final GeoIndicatorResultRepository resultRepository;

  @Autowired
  public GeographicZoneService(GeoReportingRateRepository repository,
                               GeoIndicatorResultRepository resultRepository) {
    this.repository = repository;
    this.resultRepository = resultRepository;
  }

  /**
   * Retrieves geographic zone reporting rates by program ID and period ID.
   * The reporting rates include expected, total, ever, and period counts for each geographic zone.
   *
   * @param programId          The ID of the program.
   * @param processingPeriodId The ID of the processing period.
   * @return List of GeoZoneReportingRateDto objects.
   */
  public List<GeoZoneReportingRateDto> getGeoReportingRate(String programId,
                                                           String processingPeriodId) {
    return repository.getGeoReportingRate(
        programId,
        processingPeriodId);
  }

  /**
   * Retrieves geographic zone reporting rates by program ID and period ID.
   * The reporting rates include expected, total, ever, and period counts for each geographic zone.
   *
   * @param programId          The ID of the program.
   * @param processingPeriodId The ID of the processing period.
   * @param zoneId             The zone ID of the processing period.
   * @return List of GeoFacilityIndicatorDto objects.
   */
  public List<GeoFacilityIndicatorDto> getFacilityIndicator(String programId,
                                                           String processingPeriodId,
                                                           String zoneId) {
    return resultRepository.getFacilityIndicator(
        programId,
        processingPeriodId,
        zoneId);
  }

}
