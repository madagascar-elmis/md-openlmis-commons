/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.service;

import static org.openlmis.common.i18n.MessageKeys.DUPLICATED;
import static org.openlmis.common.i18n.MessageKeys.MUST_CONTAIN_VALUE;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.QuoteMode;
import org.openlmis.common.domain.ApplicationInterface;
import org.openlmis.common.domain.InterfaceDataSet;
import org.openlmis.common.dto.ApplicationInterfaceExtensionDto;
import org.openlmis.common.dto.DataSetDto;
import org.openlmis.common.exception.ValidationMessageException;
import org.openlmis.common.repository.ApplicationInterfaceRepository;
import org.openlmis.common.repository.DataSetInterfaceRepository;
import org.openlmis.common.util.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * Service class for managing application interfaces and data sets.
 */
@Service
public class DataService {

  @Autowired
  private ApplicationInterfaceRepository repository;

  @Autowired
  private DataSetInterfaceRepository dataSetInterfaceRepository;

  private static final String[] ITERFACE_DATASET_HEADERS = {
      "Dataset Code", "Dataset Id", "Interface Code",
  };

  private void validateRequiredValueNotNull(ApplicationInterface reason) {
    if (reason.getName().isEmpty()) {
      throwException(MUST_CONTAIN_VALUE);
    }
  }

  private void throwException(String errorKey) {
    throw new ValidationMessageException(new Message(errorKey));
  }

  /**
   * Check if updating reason would cause a duplicate with another one.
   * Throws an exception without updating itself.
   *
   * @param reason The application interface to be updated
   */
  private void validateReasonNameDuplicate(
      ApplicationInterface reason) {
    ApplicationInterface foundReason = repository
        .findByName(reason.getName());

    if (foundReason != null && !foundReason.getId()
        .equals(reason.getId())) {
      throwException(DUPLICATED);
    }
  }

  /**
   * Saves a new application interface with the provided data sets.
   *
   * @param interfaceDto The application interface DTO
   */
  @Transactional
  public void saveData(ApplicationInterfaceExtensionDto
                             interfaceDto) {

    ApplicationInterface applicationInterface = new
        ApplicationInterface();
    applicationInterface.setName(interfaceDto.getName());
    applicationInterface.setActive(interfaceDto.getActive());

    validateRequiredValueNotNull(applicationInterface);
    validateReasonNameDuplicate(applicationInterface);

    List<InterfaceDataSet> dataSets = new ArrayList<>();

    for (DataSetDto dataSetDto : interfaceDto.getDataSets()) {
      InterfaceDataSet dataSet = new InterfaceDataSet();
      dataSet.setName(dataSetDto.getName());
      dataSet.setValue(dataSetDto.getValue());
      dataSet.setApplicationInterface(applicationInterface);
      dataSets.add(dataSet);
    }

    applicationInterface.setDataSets(dataSets);

    repository.save(applicationInterface);
  }

  /**
   * Deletes the application interface with the specified ID.
   *
   * @param id The ID of the application interface to delete
   */
  @Transactional
  public void deleteData(UUID id) {
    repository.deleteById(id);
  }

  /**
   * Retrieves all application interfaces.
   *
   * @return A list of application interface DTOs
   */
  public List<ApplicationInterfaceExtensionDto> getAllData() {
    List<ApplicationInterface> interfaces = repository.findAll();
    return interfaces.stream()
        .map(this::mapToDto)
        .collect(Collectors.toList());
  }

  /**
   * Retrieves the application interface with the specified ID.
   *
   * @param id The ID of the application interface to retrieve
   * @return The application interface DTO
   * @throws EntityNotFoundException if the interface with the specified ID is not found
   */
  public ApplicationInterfaceExtensionDto getDataById(UUID id) {
    ApplicationInterface applicationInterface = repository.findById(id)
        .orElseThrow(() ->
            new EntityNotFoundException(
                "Interface not found with id: " + id));
    return mapToDto(applicationInterface);
  }

  /**
   * Updates the application interface with the specified ID with the provided data sets.
   *
   * @param id           The ID of the application interface to update
   * @param interfaceDto The application interface DTO with updated data
   */
  @Transactional
  public void updateData(UUID id,
                         ApplicationInterfaceExtensionDto interfaceDto) {
    ApplicationInterface applicationInterface = repository.findById(id)
        .orElseThrow(() ->
            new EntityNotFoundException(
                "Interface not found with id: " + id));

    applicationInterface.setName(interfaceDto.getName());
    applicationInterface.setActive(interfaceDto.getActive());

    List<InterfaceDataSet> dataSets = new ArrayList<>();

    for (DataSetDto dataSetDto : interfaceDto.getDataSets()) {
      InterfaceDataSet dataSet = new InterfaceDataSet();
      dataSet.setName(dataSetDto.getName());
      dataSet.setValue(dataSetDto.getValue());
      dataSet.setApplicationInterface(applicationInterface);
      dataSets.add(dataSet);
    }
    applicationInterface.setDataSets(dataSets);

    repository.save(applicationInterface);
  }

  /**
   * Imports a dataset from a CSV file.
   *
   * @param file The CSV file containing the dataset.
   */
  public void importDatasetToDB(MultipartFile file) {

    try (BufferedReader reader = new BufferedReader(
        new InputStreamReader(file.getInputStream()))) {

      CSVParser parser = CSVFormat.DEFAULT.withHeader().parse(reader);

      List<String> headers = new ArrayList<>(parser
          .getHeaderMap().keySet());

      for (CSVRecord record : parser) {
        if (!record.isConsistent()) {
          continue;
        }
        String name = record.get(headers.get(0));
        String value = record.get(headers.get(1));
        String interfaceCode = record.get(headers.get(2));
        saveDataset(name, value, interfaceCode);

      }

    } catch (IOException e) {
      throw new ValidationMessageException(e,
          "fail to import data to CSV file: " + e.getMessage());
    }

  }

  /**
   * Save a dataset from a CSV file.
   *
   * @param name The CSV file containing the dataset.
   * @param value The CSV file containing the dataset.
   * @param code The CSV file containing the dataset.
   */
  public void saveDataset(String name, String value, String code) {

    ApplicationInterface
        applicationInterface = repository.findByName(code);

    if (applicationInterface == null) {
      return;
    }
    InterfaceDataSet dataSet = getDatasetByCodeAndInterface(
        name, applicationInterface);
    dataSet.setName(name);
    dataSet.setValue(value);
    dataSet.setApplicationInterface(applicationInterface);
    dataSetInterfaceRepository.save(dataSet);
  }

  /**
   * Retrieves a dataset by its name and application interface.
   *
   * @param name      The name of the dataset.
   * @param applicationInterface The application interface associated with the dataset.
   * @return The dataset if found, or an empty dataset if not found.
   */
  public InterfaceDataSet getDatasetByCodeAndInterface(
      String name, ApplicationInterface applicationInterface) {

    return dataSetInterfaceRepository.findByNameAndApplicationInterface(
        name, applicationInterface).orElse(new InterfaceDataSet());
  }

  /**
   * Generates a sample CSV file for importing datasets.
   *
   * @return A ByteArrayInputStream containing the sample CSV data.
   */
  public ByteArrayInputStream generateSampleImportCsv() {

    final CSVFormat format = CSVFormat
        .DEFAULT.withQuoteMode(QuoteMode.MINIMAL)
        .withHeader(ITERFACE_DATASET_HEADERS);

    try {
      ByteArrayOutputStream out = new ByteArrayOutputStream();
      CSVPrinter csvPrinter = new CSVPrinter(
          new PrintWriter(out), format);

      csvPrinter.flush();
      return new ByteArrayInputStream(out.toByteArray());
    } catch (IOException e) {
      throw new ValidationMessageException(e,
          "fail to export data to CSV file: " + e.getMessage());
    }
  }

  /**
   * Maps an ApplicationInterface entity to its corresponding DTO.
   *
   * @param applicationInterface The ApplicationInterface entity to map.
   * @return The mapped ApplicationInterfaceExtensionDto DTO.
   */
  private ApplicationInterfaceExtensionDto mapToDto(
      ApplicationInterface applicationInterface) {
    ApplicationInterfaceExtensionDto dto = new
        ApplicationInterfaceExtensionDto();
    dto.setId(applicationInterface.getId());
    dto.setName(applicationInterface.getName());
    dto.setActive(applicationInterface.getActive());
    dto.setDataSets(applicationInterface.getDataSets().stream()
        .map(dataSet -> new DataSetDto(dataSet.getId(),
            dataSet.getName(), dataSet.getValue()))
        .collect(Collectors.toList()));
    return dto;
  }
}
