/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.service;

import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.openlmis.common.domain.MsdDailyStockStatus;
import org.openlmis.common.domain.MsdDailyStockStatusValue;
import org.openlmis.common.exception.NotFoundException;
import org.openlmis.common.i18n.MsdDailyStockStatusMessageKeys;
import org.openlmis.common.repository.MsdDailyStockStatusRepository;
import org.openlmis.common.repository.MsdDailyStockStatusValueRepository;
import org.openlmis.common.util.Message;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class MsdDailyStockStatusService {

  private final MsdDailyStockStatusRepository stockStatusRepository;

  private final MsdDailyStockStatusValueRepository stockStatusValueRepository;


  /**
   * Create a new stock status using the specified details.
   *
   * @param importer details of the new stock status .
   *
   * @return
   */
  @Transactional
  public MsdDailyStockStatus create(MsdDailyStockStatus.Importer importer) {

    deleteAllStockStatus();

    MsdDailyStockStatus msdDailyStockStatus = MsdDailyStockStatus.newInstance(importer);
    msdDailyStockStatus.setId(null);
    msdDailyStockStatus = stockStatusRepository.save(msdDailyStockStatus);
    return msdDailyStockStatus;
  }


  /**
   * Retrieve stock status of specified Id.
   *
   * @param stockStatusId id of the stock status to retrieve.
   *
   * @return
   */
  public MsdDailyStockStatus getStockStatus(UUID stockStatusId) {
    MsdDailyStockStatus msdDailyStockStatus = stockStatusRepository.findById(stockStatusId)
        .orElseThrow(
            () -> new NotFoundException(new Message(MsdDailyStockStatusMessageKeys.ERROR_NOT_FOUND))
        );
    return msdDailyStockStatus;
  }

  /**
   * Retrieve stock status value of specified Id.
   *
   * @param stockStatusValueId id of the stock status to retrieve.
   *
   * @return
   */
  public MsdDailyStockStatusValue getStockStatusValue(UUID stockStatusValueId) {
    MsdDailyStockStatusValue value =
        stockStatusValueRepository.findById(stockStatusValueId)
            .orElseThrow(() -> new NotFoundException(
                    new Message(MsdDailyStockStatusMessageKeys.ERROR_NOT_FOUND)
                )
            );
    return value;
  }

  /**
   * Find all available stock statuse values paginated.
   *
   * @param pageable pagination details.
   *
   * @return
   */
  public Page<MsdDailyStockStatusValue> findAllStockStatusValue(Pageable pageable) {
    return stockStatusValueRepository.findAll(pageable);
  }


  /**
   * Find all available stock statuses paginated.
   *
   * @param pageable pagination details.
   *
   * @return
   */
  public Page<MsdDailyStockStatus> findAllStockStatus(Pageable pageable) {
    return stockStatusRepository.findAll(pageable);
  }

  /**
   * Delete all available stock statuses.
   */
  public void deleteAllStockStatus() {
    stockStatusRepository.deleteAll();
  }

  /**
   * Search stock status values based on the plant filter.
   *
   * @param plant    plant search parameter value.
   * @param pageable pagination information.
   *
   * @return a page of found stock status values which conforms the search parameters.
   */
  public Page<MsdDailyStockStatusValue> searchStockStatusValue(String plant, Pageable pageable) {
    return stockStatusValueRepository.findAllByPlantContainingIgnoreCase(plant, pageable);
  }

}
