/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.service.requisition;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import org.openlmis.common.dto.requisition.RequisitionPeriodDto;
import org.openlmis.common.service.RequestParameters;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Service
public class RequisitionPeriodService extends BaseRequisitionService<RequisitionPeriodDto> {

  public static final String ACCESS_TOKEN = "access_token";

  /**
   * Retrieves access token from the current HTTP context.
   *
   * @return token.
   */
  public String obtainUserAccessToken() {
    HttpServletRequest request = getCurrentHttpRequest();
    if (request == null) {
      return null;
    }

    return request.getParameter(ACCESS_TOKEN);
  }

  private HttpServletRequest getCurrentHttpRequest() {
    RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
    if (requestAttributes instanceof ServletRequestAttributes) {
      return ((ServletRequestAttributes) requestAttributes).getRequest();
    }
    return null;
  }

  @Override
  protected String getUrl() {
    return "/api/requisitions/";
  }

  @Override
  protected Class<RequisitionPeriodDto> getResultClass() {
    return RequisitionPeriodDto.class;
  }

  @Override
  protected Class<RequisitionPeriodDto[]> getArrayResultClass() {

    return RequisitionPeriodDto[].class;

  }

  /**
   * This method retrieves Facilities with facilityName similar with name parameter or
   * facilityCode similar with code parameter.
   *
   * @param programId Field with string to find similar code.
   * @param facilityId Filed with string to find similar name.
   * @return List of FacilityDtos with similar code or name.
   */
  public List<RequisitionPeriodDto> search(UUID programId, UUID facilityId, boolean emergency) {
    Map<String, Object> requestBody = new HashMap<>();
    requestBody.put("programId", programId);
    requestBody.put("facilityId", facilityId);
    requestBody.put("emergency", emergency);

    RequestParameters parameters = RequestParameters.init();
    parameters.set("facilityId", facilityId);
    parameters.set("programId", programId);
    parameters.set("emergency", emergency)
        .set(ACCESS_TOKEN, obtainUserAccessToken());
    String url = "periodsForInitiate";
    return findAll(url, parameters);

  }

  protected Page<RequisitionPeriodDto> getBasicPeriodPage(String resourceUrl,
                                                          RequestParameters parameters,
                                                          Object payload) {
    return getPage(resourceUrl, parameters, payload, HttpMethod.GET, RequisitionPeriodDto.class);
  }
}
