/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.service.requisition;

import java.util.UUID;
import org.openlmis.common.dto.IntegrationLogDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;

@Component("LogService")
public class LogService {

  private static final String UPDATED = "Updated";

  @Autowired
  private IntegrationLogService integrationLogService;

  /**
   * Async class to save logs after the response has been returned.
   */
  @Async
  public void logError(HttpClientErrorException ex, String sourceOrderId,
                       UUID requisitionId, String type) {
    IntegrationLogDto logDto;
    if (ex.getRawStatusCode() == 200) {

      logDto = prepareSave(UPDATED, sourceOrderId, requisitionId,
          ex.getStatusCode().value(), true, type);
    } else {
      logDto = prepareSave(ex.getMessage(), sourceOrderId, requisitionId,
          ex.getRawStatusCode(), false, type);
    }
    integrationLogService.saveLog(logDto);
  }

  private IntegrationLogDto prepareSave(String errorMessage,
                                        String sourceTransactionId,
                                        UUID destinationId,
                                        Integer errorCode,
                                        Boolean resolved,
                                        String integrationName) {
    IntegrationLogDto log = new IntegrationLogDto();
    log.setErrorMessage(errorMessage);
    log.setIntegrationName(integrationName);
    log.setErrorMessage(errorMessage);
    log.setResolved(resolved);
    log.setSourceTransactionId(sourceTransactionId);
    log.setDestinationId(destinationId);
    log.setErrorCode(errorCode);

    return log;
  }

}
