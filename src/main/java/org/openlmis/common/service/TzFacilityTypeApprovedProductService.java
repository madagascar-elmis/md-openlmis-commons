/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.service;

import java.util.List;
import java.util.UUID;
import org.openlmis.common.dto.FacilityTypeApprovedProductDto;
import org.openlmis.common.repository.TzFacilityTypeApprovedProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TzFacilityTypeApprovedProductService {

  @Autowired
  private TzFacilityTypeApprovedProductRepository repository;

  /**
   * Updates the facility type
   * approved product with the provided DTO.
   *
   * @param dto The DTO containing the updated information.
   * @param facilityTypeApprovedProductId The UUID of the approved products.
   * @return
   */
  public FacilityTypeApprovedProductDto updateFacilityTypeApproved(
      FacilityTypeApprovedProductDto dto,
      UUID facilityTypeApprovedProductId) {

    dto.setId(facilityTypeApprovedProductId.toString());
    repository.updatePriorityDrug(dto);

    return dto;
  }

  /**
   * Retrieves a list of FacilityTypeApprovedProductDto objects by their UUID.
   *
   * @param id The UUID of the facility type approved product(s) to retrieve.
   * @return A list of objects matching the given UUID.
   */
  public List<FacilityTypeApprovedProductDto> getGetApprovedBy(UUID id) {
    return repository.findById(id);
  }

  /**
   * Retrieves a list of FacilityTypeApprovedProductDto objects.
   *
   * @return A list of objects.
   */
  public Iterable<FacilityTypeApprovedProductDto> findAllData() {
    return repository.findAllData();
  }
}
