/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.service;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.analyticsreporting.v4.AnalyticsReporting;
import com.google.api.services.analyticsreporting.v4.AnalyticsReportingScopes;
import com.google.api.services.analyticsreporting.v4.model.DateRange;
import com.google.api.services.analyticsreporting.v4.model.Dimension;
import com.google.api.services.analyticsreporting.v4.model.GetReportsRequest;
import com.google.api.services.analyticsreporting.v4.model.GetReportsResponse;
import com.google.api.services.analyticsreporting.v4.model.Metric;
import com.google.api.services.analyticsreporting.v4.model.Report;
import com.google.api.services.analyticsreporting.v4.model.ReportRequest;
import com.google.api.services.analyticsreporting.v4.model.ReportRow;
import java.io.IOException;
import java.io.Writer;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.openlmis.common.dto.UserContactDetailsDto;
import org.openlmis.common.dto.VisitorDto;
import org.openlmis.common.dto.referencedata.FacilityDto;
import org.openlmis.common.dto.referencedata.UserDto;
import org.openlmis.common.service.notification.UserContactDetailsService;
import org.openlmis.common.service.referencedata.FacilityReferenceDataService;
import org.openlmis.common.service.referencedata.UserReferenceDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

@Service
public class GoogleAnalyticsService {

  private static final JsonFactory JSON_FACTORY = GsonFactory.getDefaultInstance();

  @Value("${google.analytics.key.file.location}")
  private Resource keyFile;

  @Value("${google.analytics.application.name}")
  private String applicationName;

  @Value("${google.analytics.view.id}")
  private String viewId;

  @Value("${google.analytics.view.visitors.dimension}")
  private String visitorsReportDimension;

  @Value("${google.analytics.view.visitors.metric}")
  private String visitorsReportMetric;

  @Autowired
  private UserReferenceDataService userReferenceDataService;

  @Autowired
  private UserContactDetailsService userContactDetailsService;

  @Autowired
  private FacilityReferenceDataService facilityReferenceDataService;

  /**
   * Generate visitor report in CSV and write using the provided Writer.
   *
   * @param startDate A filter of start date.
   * @param endDate   A filter of end date.
   *
   * @throws java.io.IOException IO Exception
   */
  public void generateVisitorsReport(String startDate, String endDate, Writer writer)
      throws IOException, GeneralSecurityException {

    List<VisitorDto> visitorDtos = getVisitorsReport(startDate, endDate);

    CSVFormat csvFormat = CSVFormat.DEFAULT.withHeader(
        "Username",
        "First Name",
        "Last Name",
        "Job Title",
        "Email",
        "Phone Number",
        "Page Views",
        "District",
        "Region",
        "Zone",
        "Facility Code",
        "Facility Name"
    );

    try (CSVPrinter printer = new CSVPrinter(writer, csvFormat)) {
      for (VisitorDto visitorDto : visitorDtos) {
        boolean hasUserData = visitorDto.getUserDto() != null;
        if (!hasUserData) {
          continue;
        }

        boolean hasFacilityData = visitorDto.getFacilityDto() != null;
        boolean hasDistrict = hasFacilityData && visitorDto.getDistrict() != null;
        boolean hasRegion = hasDistrict && visitorDto.getRegion() != null;
        boolean hasZone = hasRegion && visitorDto.getZone() != null;
        boolean hasContactData = visitorDto.getUserContactDetailsDto() != null;

        printer.printRecord(
            visitorDto.getUserDto().getUsername(),
            visitorDto.getUserDto().getFirstName(),
            visitorDto.getUserDto().getLastName(),
            visitorDto.getUserDto().getJobTitle(),
            hasContactData
                ? visitorDto.getUserContactDetailsDto().getEmailDetails().getEmail()
                : "",
            hasContactData
                ? visitorDto.getUserContactDetailsDto().getPhoneNumber()
                : "",
            visitorDto.getPageView(),
            hasDistrict ? visitorDto.getDistrict().getName() : "",
            hasRegion ? visitorDto.getRegion().getName() : "",
            hasZone ? visitorDto.getZone().getName() : "",
            hasFacilityData ? visitorDto.getFacilityDto().getCode() : "",
            hasFacilityData ? visitorDto.getFacilityDto().getName() : ""
        );
      }
    }
  }

  /**
   * Queries the Analytics Reporting API V4.
   *
   * @param startDate A filter of start date.
   * @param endDate   A filter of end date.
   *
   * @return visitor summary entries
   *
   * @throws java.io.IOException IO Exception
   */
  public List<VisitorDto> getVisitorsReport(String startDate, String endDate)
      throws IOException, GeneralSecurityException {
    final AnalyticsReporting service = initializeAnalyticsReporting();

    DateRange dateRange = new DateRange();
    dateRange.setStartDate(startDate);
    dateRange.setEndDate(endDate);

    // Create the Metrics object.
    Metric sessions = new Metric()
        .setExpression(visitorsReportMetric);
    Dimension pageTitle = new Dimension().setName(visitorsReportDimension);

    // Create the ReportRequest object.
    ReportRequest request = new ReportRequest()
        .setViewId(viewId)
        .setDateRanges(Arrays.asList(dateRange))
        .setMetrics(Arrays.asList(sessions))
        .setDimensions(Arrays.asList(pageTitle));

    ArrayList<ReportRequest> requests = new ArrayList<ReportRequest>();
    requests.add(request);

    // Create the GetReportsRequest object.
    GetReportsRequest getReport = new GetReportsRequest()
        .setReportRequests(requests);

    // Call the batchGet method.
    GetReportsResponse response = service.reports().batchGet(getReport).execute();


    return parseResponse(response);
  }

  /**
   * Initializes an Analytics Reporting API V4 service object.
   *
   * @return An authorized Analytics Reporting API V4 service object.
   *
   * @throws java.io.IOException                    IO Exception
   * @throws java.security.GeneralSecurityException Security Exception
   */
  public AnalyticsReporting initializeAnalyticsReporting()
      throws GeneralSecurityException, IOException {

    HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();

    GoogleCredential credential = GoogleCredential
        .fromStream(keyFile.getInputStream())
        .createScoped(AnalyticsReportingScopes.all());
    // Construct the Analytics Reporting service object.
    return new AnalyticsReporting.Builder(httpTransport, JSON_FACTORY, credential)
        .setApplicationName(applicationName).build();
  }

  /**
   * Parses and prints the Analytics Reporting API V4 response.
   *
   * @param response .
   */
  private List<VisitorDto> parseResponse(GetReportsResponse response) {

    ArrayList<VisitorDto> visitorDtos = new ArrayList<>();

    for (Report report : response.getReports()) {
      List<ReportRow> rows = report.getData().getRows();

      if (rows == null) {
        continue;
      }

      for (ReportRow row : rows) {
        UUID userId = UUID.fromString(row.getDimensions().get(0));
        String pageViews = row.getMetrics().get(0).getValues().get(0);

        VisitorDto visitorDto = new VisitorDto();
        visitorDto.setId(userId);
        visitorDto.setPageView(pageViews);

        UserDto userDto = userReferenceDataService.findOne(visitorDto.getId());
        visitorDto.setUserDto(userDto);

        UserContactDetailsDto contactDetailsDto = userContactDetailsService.findOne(userId);
        visitorDto.setUserContactDetailsDto(contactDetailsDto);

        UUID homeFacilityId = userDto.getHomeFacilityId();
        if (homeFacilityId != null) {
          FacilityDto facilityDto = facilityReferenceDataService.findOne(homeFacilityId);
          visitorDto.setFacilityDto(facilityDto);
        }

        visitorDtos.add(visitorDto);
      }
    }

    return visitorDtos;
  }
}
