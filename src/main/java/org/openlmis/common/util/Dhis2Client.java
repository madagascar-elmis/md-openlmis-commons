/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.cert.X509Certificate;
import java.util.Base64;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import lombok.AllArgsConstructor;
import org.openlmis.common.dto.integration.ElmisInterfaceDto;
import org.openlmis.common.service.integration.ExportLogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * The Dhis2Client class provides functionality to push data to a DHIS2 instance.
 */
@Component
@AllArgsConstructor
public class Dhis2Client {
  private static final Logger logger = LoggerFactory.getLogger(Dhis2Client.class);

  private final ExportLogService exportLogService;

  /**
   * Pushes data to a DHIS2 instance using the provided credentials and URL.
   *
   * @param username the username for DHIS2 authentication
   * @param password the password for DHIS2 authentication
   * @param url      the URL of the DHIS2 endpoint
   * @param data     the data to be pushed to DHIS2
   */
  public void pushDataToDhis(String username, String password, String url, ElmisInterfaceDto data) {
    ObjectMapper mapper = new ObjectMapper();
    disableSslVerification();
    try {
      URL obj = new URL(url);
      HttpURLConnection con = (HttpURLConnection) obj.openConnection();

      String jsonInString = mapper.writeValueAsString(data);
      logger.debug("Request payload: {}", jsonInString);

      String userCredentials = username + ":" + password;
      String basicAuth = "Basic " + Base64.getEncoder()
              .encodeToString(userCredentials.getBytes(StandardCharsets.UTF_8));

      con.setRequestProperty("Authorization", basicAuth);
      con.setRequestMethod("POST");
      con.setRequestProperty("Content-Type", "application/json");
      con.setDoOutput(true);
      con.setConnectTimeout(15000); // 15 seconds
      con.setReadTimeout(15000); // 15 seconds

      try (OutputStream wr = con.getOutputStream()) {
        wr.write(jsonInString.getBytes(StandardCharsets.UTF_8));
        wr.flush();
      }

      int responseCode = con.getResponseCode();
      logger.info("Response Code: {}", responseCode);

      StringBuilder response;
      try (BufferedReader in = new BufferedReader(
              new InputStreamReader(con.getInputStream(), StandardCharsets.UTF_8))) {
        String inputLine;
        response = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
          response.append(inputLine);
        }
      }

      String responseString = response.toString();
      logger.info("Response from DHIS2: {}", responseString);
      exportLogService.saveResponseAsExportLog(responseString, data);

    } catch (MalformedURLException e) {
      logger.error("Malformed URL: {}", url, e);
    } catch (IOException e) {
      logger.error("I/O error while sending data to DHIS2", e);
    }
  }

  /**
   * Disables SSL certificate verification. This is useful for testing purposes only.
   */
  private static void disableSslVerification() {
    try {
      TrustManager[] trustAllCerts = new TrustManager[]{
          new X509TrustManager() {
              public X509Certificate[] getAcceptedIssuers() {
                return null;
              }

              public void checkClientTrusted(X509Certificate[] certs, String authType) {
              }

              public void checkServerTrusted(X509Certificate[] certs, String authType) {
              }
            }
      };

      SSLContext sc = SSLContext.getInstance("SSL");
      sc.init(null, trustAllCerts, new java.security.SecureRandom());
      HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

      HostnameVerifier allHostsValid = (hostname, session) -> true;
      HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
    } catch (Exception e) {
      logger.error("Failed to disable SSL verification", e);
    }
  }
}
