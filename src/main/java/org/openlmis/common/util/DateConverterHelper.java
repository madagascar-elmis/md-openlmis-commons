/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.util;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.stereotype.Component;

@Component
public class DateConverterHelper {

  DateTimeFormatter formatter;

  /**
   * Provide the local Date from given string.
   *
   * @param stringDate stringDate to be formatted
   *
   * @return returns local Date value
   */
  public LocalDate getFormattedDateFromString(String stringDate) {

    formatter = DateTimeFormat.forPattern("yyyy-MM-dd");

    DateTime dt = formatter.parseDateTime(stringDate);

    return dt.toLocalDate();

  }

}
