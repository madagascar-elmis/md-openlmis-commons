/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.dto;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.openlmis.common.domain.StockNotificationLineItem;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class StockNotificationLineItemDto
    implements StockNotificationLineItem.Importer, StockNotificationLineItem.Exporter {

  private UUID id;

  private String itemCode;

  private String itemDescription;

  private String uom;

  private Integer quantity;

  private Integer quantityShipped;

  private Integer quantityOrdered;

  private String missingItemStatus;

  private String dueDate;

  /**
   * Create new instance of StockNotificationLineItemDto based on given
   * {@link StockNotificationLineItem}.
   *
   * @param lineItem instance of parameter lineItem.
   *
   * @return new instance of StockNotificationLineItemDto.
   */
  public static StockNotificationLineItemDto newInstance(
      StockNotificationLineItem lineItem
  ) {
    StockNotificationLineItemDto dto
        = new StockNotificationLineItemDto();
    lineItem.export(dto);
    return dto;
  }

  /**
   * Creates new set of StockNotificationLineItemDto based on
   * {@link StockNotificationLineItem} iterable.
   */
  public static Set<StockNotificationLineItemDto> newInstance(
      Iterable<StockNotificationLineItem> iterable) {
    Set<StockNotificationLineItemDto> notificationDtos = new HashSet<>();
    iterable.forEach(i -> notificationDtos.add(newInstance(i)));
    return notificationDtos;
  }

}
