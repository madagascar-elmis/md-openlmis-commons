/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.dto.requisition.response;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.openlmis.common.dto.requisition.RequisitionDto;

@Getter
@Setter
@NoArgsConstructor
public class RequisitionSummaryDto {

  private String sourceOrderId;
  private UUID rnrId;
  private String facilityCode;
  private String facilityName;
  private String programCode;
  private UUID periodId;
  private String clientSubmittedTime;
  private String sourceApplication;
  private Boolean emergency;
  private String status;
  private String clientSubmittedNotes;
  private BigDecimal allocatedBudget;
  private Double totalAllocatedBudget;
  private List<ProductDto> products = new ArrayList<>();
  private List<SourceOfFundDto> sourceOfFunds = new ArrayList<>();

  /**
   * requisition date to be initiated.
   *
   * @param requisition parameter to be initialized.
   */
  private void initialize(RequisitionDto requisition) {

    this.rnrId = requisition.getId();
    this.facilityCode = requisition.getFacility().getCode();
    this.programCode = requisition.getProgram().getCode();
    this.periodId = requisition.getProcessingPeriod().getId();
    this.emergency = requisition.getEmergency();
    this.status = requisition.getStatus().name();
    this.allocatedBudget = requisition.getAllocatedBudget();
  }

}
