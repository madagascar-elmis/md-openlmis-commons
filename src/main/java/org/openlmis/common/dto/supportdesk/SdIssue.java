/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.dto.supportdesk;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class SdIssue {

  @Setter
  @Getter
  private String issueType;

  @Setter
  @Getter
  private String summary;

  @Setter
  @Getter
  private String description;

  @Setter
  @Getter
  private String raiseOnBehalfOf;

  @Setter
  @Getter
  private String priority;

  @Setter
  @Getter
  private String impact;

  @Setter
  @Getter
  private String type;

  /**
   * Constructs the SdIssue.
   *
   * @param issueType       issueType the parameter that we were trying to retrieve
   * @param type            the parameter that we were trying to retrieve
   * @param summary         summary parameter
   * @param description     description string parameter
   * @param raiseOnBehalfOf raiseOnBehalfOf string parameter
   * @param priority        priority string parameter
   * @param impact          impact string parameter
   */
  public SdIssue(String issueType, String type, String summary,
                 String description, String raiseOnBehalfOf,
                 String priority, String impact) {
    this.type = type;
    this.summary = summary;
    this.description = description;
    this.raiseOnBehalfOf = raiseOnBehalfOf;
    this.priority = priority;
    this.impact = impact;
  }

}
