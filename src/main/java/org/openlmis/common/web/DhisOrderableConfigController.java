/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.web;

import static org.openlmis.common.i18n.MessageKeys.NOT_FOUND;
import static org.openlmis.common.web.DhisOrderableConfigController.RESOURCE_PATH;

import java.util.UUID;
import org.openlmis.common.domain.DhisOrderableConfig;
import org.openlmis.common.exception.NotFoundException;
import org.openlmis.common.repository.DhisOrderableConfigRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
@RequestMapping(RESOURCE_PATH)
@Transactional
public class DhisOrderableConfigController extends BaseController {

  private static final Logger LOGGER =
      LoggerFactory.getLogger(DhisOrderableConfigController.class);

  public static final String RESOURCE_PATH = API_PATH + "/dhisOrderableConfigs";

  @Autowired
  private DhisOrderableConfigRepository repository;

  /**
   * Allows creating new configuration.
   *
   * @param config A dhisOrderableConfig bound to the request body.
   * @return the created dhisOrderableConfig.
   */
  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  @ResponseBody
  public DhisOrderableConfig createDhisOrdeableConfig(
      @RequestBody DhisOrderableConfig config) {

    LOGGER.debug("Creating new configurations");
    // Ignore provided id
    config.setId(null);
    repository.save(config);
    return config;
  }

  /**
   * Get all configurations.
   *
   * @return DhisOrderableConfig.
   */
  @GetMapping
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public Iterable<DhisOrderableConfig> geAllConfigurations() {

    Iterable<DhisOrderableConfig> configs = repository.findAll();

    if (configs == null) {
      throw new NotFoundException(NOT_FOUND);
    } else {
      return configs;
    }
  }

  /**
   * Allows updating configurations.
   *
   * @param id A dhisOrderableConfig bound to the request body.
   * @return the updated dhisOrderableConfig.
   */
  @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public DhisOrderableConfig update(
      @RequestBody DhisOrderableConfig config, @PathVariable("id") UUID id) {

    LOGGER.debug("Updating dhisOrderableConfig");
    config.setId(id);
    repository.save(config);
    return config;
  }

  /**
   * Get chosen dhisOrderableConfig.
   *
   * @param id UUID of dhisOrderableConfig which we want to get
   * @return the dhisOrderableConfig.
   */
  @RequestMapping(value = "/{id}", method = RequestMethod.GET)
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public DhisOrderableConfig getDhisOrderableConfig(
      @PathVariable("id") UUID id) {

    return repository.findById(id)
        .orElseThrow(() -> new NotFoundException(NOT_FOUND));
  }

  /**
   * Allows deleting dhisOrderableConfig.
   *
   * @param id UUID of dhisOrderableConfig which we want to delete
   */
  @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void delete(
      @PathVariable("id") UUID id) {

    DhisOrderableConfig config = repository.findById(id)
        .orElseThrow(() -> new NotFoundException(NOT_FOUND));
    repository.delete(config);
  }

}