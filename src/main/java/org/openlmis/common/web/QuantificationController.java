/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.web;

import java.io.InputStream;
import java.time.ZonedDateTime;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.openlmis.common.service.PermissionService;
import org.openlmis.common.service.requisition.QuantificationService;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
@RequiredArgsConstructor
public class QuantificationController extends BaseController {

  private final QuantificationService quantificationService;
  private final PermissionService permissionService;

  /**
   * Generates a CSV with district level quantifications report.
   *
   * @param district          A district to generate report for.
   * @param program           A program filter for the entries to be included.
   * @param period            Processing period filter for the entries to be included.
   * @param initiatedDateFrom A beginning date filter for the entries to be included.
   * @param initiatedDateTo   An end date filter for the entries to be included.
   * @return
   */
  @GetMapping(value = "/districtLevelQuantifications")
  @ResponseBody
  @ResponseStatus(HttpStatus.OK)
  public ResponseEntity<Resource> districtLevelQuantifications(
      @RequestParam(value = "district", required = true) UUID district,
      @RequestParam(value = "program", required = false) UUID program,
      @RequestParam(value = "processingPeriod", required = false) UUID period,
      @RequestParam(value = "initiatedDateFrom", required = false)
          ZonedDateTime initiatedDateFrom,
      @RequestParam(value = "initiatedDateTo", required = false)
          ZonedDateTime initiatedDateTo) {
    permissionService.canViewDistrictQuantifications(program, district);

    InputStream reportStream = quantificationService.generateDistrictLevelQuantificationCsv(
        district,
        program,
        period,
        initiatedDateFrom,
        initiatedDateTo
    );

    return ResponseEntity.ok()
        .header(HttpHeaders.CONTENT_DISPOSITION,
            "attachment; filename=district-quantifications.csv")
        .contentType(MediaType.parseMediaType("application/csv"))
        .body(new InputStreamResource(reportStream));

  }

  /**
   * Generates a CSV with facility level quantifications report.
   *
   * @param facility          A facility to generate report for.
   * @param program           A program filter for the entries to be included.
   * @param period            Processing period filter for the entries to be included.
   * @param initiatedDateFrom A beginning date filter for the entries to be included.
   * @param initiatedDateTo   An end date filter for the entries to be included.
   * @return
   */
  @GetMapping(value = "/facilityLevelQuantifications")
  @ResponseBody
  @ResponseStatus(HttpStatus.OK)
  public ResponseEntity<Resource> facilityLevelQuantifications(
      @RequestParam(value = "facility", required = true) UUID facility,
      @RequestParam(value = "program", required = false) UUID program,
      @RequestParam(value = "period", required = false) UUID period,
      @RequestParam(value = "initiatedDateFrom", required = false)
          ZonedDateTime initiatedDateFrom,
      @RequestParam(value = "initiatedDateTo", required = false)
          ZonedDateTime initiatedDateTo) {
    permissionService.canViewFacilityQuantifications(program, facility);

    InputStream reportStream = quantificationService.generateFacilityLevelQuantificationCsv(
        facility,
        program,
        period,
        initiatedDateFrom,
        initiatedDateTo
    );

    return ResponseEntity.ok()
        .header(HttpHeaders.CONTENT_DISPOSITION,
            "attachment; filename=facility-quantifications.csv"
        ).contentType(MediaType.parseMediaType("application/csv"))
        .body(new InputStreamResource(reportStream));

  }
}
