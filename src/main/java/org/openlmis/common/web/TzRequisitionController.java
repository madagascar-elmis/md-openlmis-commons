/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.web;

import java.util.UUID;
import lombok.NoArgsConstructor;
import org.openlmis.common.dto.RequisitionApprovalFinalResponseDto;
import org.openlmis.common.dto.referencedata.SupervisoryNodeDto;
import org.openlmis.common.dto.referencedata.UserDto;
import org.openlmis.common.dto.requisition.RequisitionDto;
import org.openlmis.common.exception.NotFoundException;
import org.openlmis.common.i18n.MessageKeys;
import org.openlmis.common.service.referencedata.SupervisoryNodeReferenceDataService;
import org.openlmis.common.service.requisition.RequisitionService;
import org.openlmis.common.util.AuthenticationHelper;
import org.openlmis.common.util.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@NoArgsConstructor
@RequestMapping(TzRequisitionController.RESOURCE_PATH2)
public class TzRequisitionController extends BaseController {

  private static final Logger LOGGER = LoggerFactory.getLogger(TemplateController.class);

  public static final String RESOURCE_PATH2 = API_PATH + "/tzRequisitions";

  @Autowired
  private RequisitionService requisitionService;

  @Autowired
  private SupervisoryNodeReferenceDataService supervisoryNodeReferenceDataService;

  @Autowired
  AuthenticationHelper authenticationHelper;

  /**
   * Approve specified by id requisition.
   */
  @PostMapping("/{id}/approve")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public RequisitionApprovalFinalResponseDto approveRequisition(
      @PathVariable("id") UUID requisitionId) {
    LOGGER.debug("Approve requisition");

    UserDto userDto = authenticationHelper.getCurrentUser();

    RequisitionDto requisition = requisitionService.findOne(requisitionId);
    System.out.println(requisition.getSupervisoryNode());

    SupervisoryNodeDto supervisoryNode = supervisoryNodeReferenceDataService
        .findSupervisoryNode(requisition.getProgram().getId(),
            requisition.getFacility().getId());
    LOGGER.debug("Get supervisory nodes and update");

    if (supervisoryNode != null) {
      LOGGER.debug("Get parent for supplying facility");
      SupervisoryNodeDto supervisoryNodeP = supervisoryNodeReferenceDataService
          .findOne(supervisoryNode.getParentNode().getId());

      SupervisoryNodeDto supervisoryNodePP = supervisoryNodeReferenceDataService
          .findOne(supervisoryNodeP.getParentNode().getId());

      UUID supplyingFacilityId = supervisoryNodePP.getFacility().getId();

      requisitionService.finalApproval(requisition, supervisoryNode, userDto,
          supplyingFacilityId, supervisoryNodePP.getId());
    } else {
      throw new NotFoundException(
          new Message(MessageKeys.ERROR_ORDER_NOT_FOUND, requisitionId));
    }

    LOGGER.debug("={}", supervisoryNode);
    RequisitionApprovalFinalResponseDto responseDto
        = new RequisitionApprovalFinalResponseDto();
    responseDto.setRequisitionId(requisitionId);
    responseDto.setSupervisoryNodeId(supervisoryNode.getParentNode().getId());

    return responseDto;

  }

}
