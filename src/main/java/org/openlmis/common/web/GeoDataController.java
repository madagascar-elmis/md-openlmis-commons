/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.web;

import static org.openlmis.common.web.GeoDataController.RESOURCE_PATH;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

import java.util.List;
import org.openlmis.common.dto.GeoFacilityIndicatorDto;
import org.openlmis.common.dto.GeoZoneReportingRateDto;
import org.openlmis.common.service.GeographicZoneService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
@RequestMapping(RESOURCE_PATH)
@Transactional
public class GeoDataController
    extends BaseController {

  public static final String RESOURCE_PATH = API_PATH + "/gis";

  private static final Logger LOGGER =
      LoggerFactory.getLogger(GeoDataController.class);

  @Autowired
  private GeographicZoneService service;

  /**
   * Generates reporting rate report.
   *
   * @param program A program filter for the entries to be included.
   * @param period  Processing period filter for the entries to be included.
   * @return GeoZoneReportingRateDto
   */
  @RequestMapping(value = "/reportingRate/{program}/{period}", method = GET)
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public List<GeoZoneReportingRateDto> geGeoReporting(
      @PathVariable(value = "program", required = true) String program,
      @PathVariable(value = "period", required = true) String period
  ) {
    List<GeoZoneReportingRateDto> maps = this.service
        .getGeoReportingRate(program, period);

    LOGGER.debug("Return Maps Geo Data");

    return maps;
  }

  /**
   * Retrieves facility indicators for a given program, processing period, and geo zone.
   *
   * @param program Unique identifier of the program.
   * @param period Processing period identifier.
   * @param zoneId Geographic zone identifier.
   * @return List of GeoFacilityIndicatorDto representing facility indicators.
   */
  @RequestMapping(value = "/reportingFacilities/{program}/{period}/{zone}", method = GET)
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public List<GeoFacilityIndicatorDto> getFacilityIndicator(
      @PathVariable(value = "program", required = true) String program,
      @PathVariable(value = "period", required = true) String period,
      @PathVariable(value = "zone", required = true) String zoneId
  ) {
    List<GeoFacilityIndicatorDto> maps = this.service
        .getFacilityIndicator(program, period, zoneId);

    LOGGER.debug("Return Maps Geo Data");

    return maps;
  }

}
