/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.web;

import static org.openlmis.common.i18n.MessageKeys.USER_NOT_FOUND;
import static org.openlmis.common.i18n.MessageKeys.USER_NOT_FOUND_BY_EMAIL;

import java.util.List;
import javax.transaction.Transactional;
import org.apache.commons.collections.CollectionUtils;
import org.openlmis.common.domain.User;
import org.openlmis.common.dto.UserContactDetailsDto;
import org.openlmis.common.exception.ValidationMessageException;
import org.openlmis.common.repository.UserRepository;
import org.openlmis.common.service.PasswordResetNotifier;
import org.openlmis.common.service.notification.UserContactDetailsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
@Transactional
@RequestMapping("/api/users")
public class UserController extends BaseController {
  private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

  @Autowired
  UserRepository userRepository;
  @Autowired
  private PasswordResetNotifier passwordResetNotifier;
  @Autowired
  UserContactDetailsService  userContactDetailsNotificationService;

  /**
   * Generates token which can be used to change user's password.
   */
  @RequestMapping(value = "/common/forgotPassword", method = RequestMethod.POST)
  @ResponseStatus(HttpStatus.OK)
  public void forgotPassword(@RequestParam(value = "email") String email,
                             @RequestParam(value = "lang", defaultValue = "en") String lang) {
    List<UserContactDetailsDto> found = userContactDetailsNotificationService.findByEmail(email);

    if (CollectionUtils.isEmpty(found)) {
      throw new ValidationMessageException(USER_NOT_FOUND_BY_EMAIL);
    }
    LOGGER.debug("lang: " + lang);
    LOGGER.debug("finding user =====>: " + found.get(0).getReferenceDataUserId());

    User user = userRepository.findById(found.get(0).getReferenceDataUserId()).orElseThrow(
        () -> new ValidationMessageException(USER_NOT_FOUND)
    );
    passwordResetNotifier.sendNotification(user);
  }

}
