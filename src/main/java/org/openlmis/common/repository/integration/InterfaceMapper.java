/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.repository.integration;

import org.openlmis.common.domain.integration.IntegrationData;
import org.openlmis.common.dto.integration.ElmisInterfaceDataSetDto;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * The InterfaceMapper class .
 */

@Repository
@Transactional
public interface InterfaceMapper {

  /**
   * The toDto method .
   */

  static ElmisInterfaceDataSetDto toDto(IntegrationData integrationData) {
    return   ElmisInterfaceDataSetDto.builder()
                .period(integrationData.getPeriod())
                .dataElement(integrationData.getDataElementId())
                .orgUnit(integrationData.getOrgUnitId())
                .value(integrationData.getValue())
                .build();
  }
}

