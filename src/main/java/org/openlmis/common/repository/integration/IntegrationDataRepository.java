/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.repository.integration;

import java.util.List;
import java.util.UUID;
import org.openlmis.common.domain.integration.IntegrationData;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;



@Repository
public interface IntegrationDataRepository extends
        JpaRepository<IntegrationData, UUID> {
  @Modifying
  @Transactional
  @Query(value = "REFRESH MATERIALIZED VIEW public.integration_data", nativeQuery = true)
  void refreshMaterializedView();

  @Query(value = "SELECT to_char(generate_series, 'YYYYMM') AS period "
          + "FROM generate_series(CAST(:startDate AS timestamp), now(), '1 month') "
          + "ORDER BY period DESC", nativeQuery = true)
  List<String> findPeriodsFromStartDate(String startDate);

  @Query(value = "SELECT * FROM public.integration_data where orgunitid "
          + "is not null and period=:period", nativeQuery = true)
  Page<IntegrationData> getPeriodConsumptionData(@Param("period") Integer period,
                                                 Pageable pageable);

}
