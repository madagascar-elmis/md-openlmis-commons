/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.repository;

import java.util.Optional;
import java.util.UUID;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.openlmis.common.domain.ProcessingPeriod;
import org.springframework.stereotype.Repository;

@Repository
public class ProcessingPeriodRepository {

  @PersistenceContext
  private EntityManager entityManager;

  /**
   * Verify that the processing period of the specified id exist.
   *
   * @param periodId id of processing period to verify.
   */
  public void verifyProcessingPeriodExists(UUID periodId) {
    String existSql = "select count(id) from referencedata.processing_periods where id='"
        + periodId
        + "'";
    Object existing = entityManager.createNativeQuery(existSql).getSingleResult();
    if (Long.valueOf(existing.toString()) <= 0L) {
      throw new IllegalArgumentException("There is no processing period with id: " + periodId);
    }
  }

  /**
   * Count requisition associated with the processing period of the specified id.
   *
   * @param periodId id of processing period to count requisitions for.
   */
  public Long countProcessingPeriodRequisitions(UUID periodId) {
    String existSql = "select count(id) from requisition.requisitions where processingperiodid='"
        + periodId
        + "'";
    Object existing = entityManager.createNativeQuery(existSql).getSingleResult();
    return Long.valueOf(existing.toString());
  }

  /**
   * Delete the processing period of the specified id.
   *
   * @param periodId id of processing period to delete.
   */
  public void deleteProcessingPeriod(UUID periodId) {
    String deleteSql = "delete from referencedata.processing_periods where id='" + periodId + "'";
    entityManager.createNativeQuery(deleteSql).executeUpdate();
  }

  /**
   * Delete the program supported of the specified id.
   *
   * @param programId  id of program supported to delete.
   * @param facilityId id of program supported to delete.
   */
  public void deleteProgramSupported(UUID programId, UUID facilityId) {
    String deleteSql = "delete from referencedata.supported_programs "
        + " where programId='" + programId + "' and facilityId='" + facilityId + "'";
    entityManager.createNativeQuery(deleteSql).executeUpdate();
  }

  /**
   * Verify that the program supported of the specified id exist.
   *
   * @param program  id of program supported to verify.
   * @param facility id of program supported to verify.
   */
  public void verifyProgramSupportedExists(UUID program, UUID facility) {
    String existSql = "select count(*) from referencedata.supported_programs"
        + " where programId='" + program + "' and facilityId='" + facility + "'";

    Object existing = entityManager.createNativeQuery(existSql).getSingleResult();
    if (Long.valueOf(existing.toString()) <= 0L) {
      throw new IllegalArgumentException("There is no program supported with id: "
          + program);
    }
  }

  /**
   * Count requisition associated with the program supported of the specified id.
   *
   * @param program  id of program supported to count requisitions for.
   * @param facility id of program supported to count requisitions for.
   */
  public Long countProgramSupportedRequisitions(UUID program, UUID facility) {

    String existSql = "select count(id) from requisition.requisitions where programId='"
        + program
        + "' and facilityId='" + facility + "'";
    Object existing = entityManager.createNativeQuery(existSql).getSingleResult();
    return Long.valueOf(existing.toString());

  }

  /**
   * Save processing period object.
   *
   * @param processingPeriod processingPeriod of processing period to save periods.
   */
  public void saveProcessingPeriod(ProcessingPeriod processingPeriod) {
    entityManager.createNativeQuery(
            "INSERT INTO referencedata.processing_periods ("
                + "id, description, endDate, name, startDate, "
                + "processingScheduleId, extradata)"
                + " VALUES (?, ?, ?, ?, ?, ?, CAST(? AS jsonb))")
        .setParameter(1, UUID.randomUUID())
        .setParameter(2, processingPeriod.getDescription())
        .setParameter(3, processingPeriod.getEndDate())
        .setParameter(4, processingPeriod.getName())
        .setParameter(5, processingPeriod.getStartDate())
        .setParameter(6, processingPeriod.getProcessingSchedule().getId())
        .setParameter(7, processingPeriod.getJsonData())
        .executeUpdate();
  }

  /**
   * get processing period object by id.
   *
   * @param periodId periodId of processing period to list periods.
   */
  public Optional<ProcessingPeriod> findById(UUID periodId) {
    try {
      ProcessingPeriod processingPeriod = entityManager
          .find(ProcessingPeriod.class, periodId);
      return Optional.ofNullable(processingPeriod);
    } catch (Exception e) {
      // Handle exceptions or log errors
      return Optional.empty();
    }
  }

}
