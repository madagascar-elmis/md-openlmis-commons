/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.domain;

import static com.fasterxml.jackson.databind.annotation.JsonSerialize.Inclusion.NON_EMPTY;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "malaria_validations", schema = "common")
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@JsonSerialize(include = NON_EMPTY)
public class MalariaValidation extends BaseEntity {

  @Getter
  @Setter
  private UUID requisitionId;

  @Getter
  @Setter
  private UUID productId;

  @Getter
  @Setter
  private String productCode;

  @Getter
  @Setter
  private String fullProductName;

  @Getter
  @Setter
  private String dispensingUnit;

  @Getter
  @Setter
  private Integer totalPatient;

  @Getter
  @Setter
  private Integer totalConsumption;

  /**
   * Constructor.
   *
   * @param requisitionId    requisitionId of the malaria validation
   * @param productId        productId of the malaria validation
   * @param productCode      productCode of the malaria validation
   * @param fullProductName  fullProductName of the malaria validation
   * @param dispensingUnit   dispensingUnit of the malaria validation
   * @param totalConsumption totalConsumption of the malaria validation
   * @param totalPatient     totalPatient of the malaria validation
   */
  public MalariaValidation(UUID requisitionId,
                           UUID productId,
                           String productCode,
                           String fullProductName,
                           String dispensingUnit,
                           Integer totalPatient,
                           Integer totalConsumption) {
    this.requisitionId = requisitionId;
    this.productId = productId;
    this.productCode = productCode;
    this.fullProductName = fullProductName;
    this.dispensingUnit = dispensingUnit;
    this.totalPatient = totalPatient;
    this.totalConsumption = totalConsumption;
  }

  /**
   * Creates new instance based on data from the importer.
   */
  public static MalariaValidation newInstance(MalariaValidation.Importer importer) {

    MalariaValidation validation = new MalariaValidation();
    validation.setRequisitionId(importer.getRequisitionId());
    validation.setProductId(importer.getProductId());
    validation.setDispensingUnit(importer.getDispensingUnit());
    validation.setProductCode(importer.getProductCode());
    validation.setTotalConsumption(importer.getTotalConsumption());
    validation.setFullProductName(importer.getFullProductName());
    validation.setTotalPatient(importer.getTotalPatient());
    return validation;
  }

  /**
   * Export this object to the specified exporter (DTO).
   *
   * @param exporter exporter to export to
   */
  public void export(MalariaValidation.Exporter exporter) {

    exporter.setId(getId());
    exporter.setRequisitionId(getRequisitionId());
    exporter.setProductCode(getProductCode());
    exporter.setProductId(getProductId());
    exporter.setDispensingUnit(getDispensingUnit());
    exporter.setFullProductName(getFullProductName());
    exporter.setTotalPatient(getTotalPatient());
    exporter.setTotalConsumption(getTotalConsumption());

  }

  public interface Exporter extends BaseExporter {

    void setProductId(UUID productId);

    void setRequisitionId(UUID requisitionId);

    void setProductCode(String productCode);

    void setFullProductName(String fullProductName);

    void setDispensingUnit(String dispensingUnit);

    void setTotalPatient(Integer totalPatient);

    void setTotalConsumption(Integer totalConsumption);

  }

  public interface Importer extends BaseExporter {
    UUID getRequisitionId();

    UUID getProductId();

    String getProductCode();

    String getFullProductName();

    String getDispensingUnit();

    Integer getTotalPatient();

    Integer getTotalConsumption();
  }

}
