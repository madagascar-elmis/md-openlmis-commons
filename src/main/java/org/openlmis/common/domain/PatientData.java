/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.domain;

import java.time.ZonedDateTime;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "dhis_patients", schema = "common")
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class PatientData extends BaseTimestampedEntity {

  @Getter
  @Setter
  private String periodId;

  @Getter
  @Setter
  private String facilityCode;

  @OneToMany(cascade = CascadeType.PERSIST, mappedBy = "patientData",
      orphanRemoval = true)
  @Setter
  @Getter
  private List<PatientDataLineItem> lineItems;

  @ManyToOne(cascade = CascadeType.PERSIST)
  @JoinColumn(name = "responseId")
  @Getter
  @Setter
  private DataResponse dataResponse;

  /**
   * Constructor.
   *
   * @param periodId      name of the interface app
   * @param facilityCode name of the interface app
   * @param dataResponse  active of the interface app
   */
  public PatientData(String periodId,
                     String facilityCode,
                     DataResponse dataResponse) {
    this.periodId = periodId;
    this.facilityCode = facilityCode;
    this.dataResponse = dataResponse;
  }

  /**
   * Creates new instance based on data from the importer.
   */
  public static PatientData newInstance(PatientData.Importer importer) {

    PatientData patientData = new PatientData();
    patientData.setDataResponse(importer.getDataResponse());
    patientData.setFacilityCode(importer.getFacilityCode());
    patientData.setLineItems(importer.getLineItems());
    patientData.setPeriodId(importer.getPeriodId());
    patientData.setCreatedDate(importer.getCreatedDate());
    patientData.setModifiedDate(importer.getModifiedDate());

    return patientData;
  }

  /**
   * Copy values of attributes into new or updated ApplicationInterface.
   *
   * @param items list of interface data sets.
   */
  public void updateFrom(List<PatientDataLineItem> items) {

    if (items != null) {
      lineItems.clear();
      lineItems.addAll(items);
      setModifiedDate(ZonedDateTime.now());
    }

  }

  /**
   * Export this object to the specified exporter (DTO).
   *
   * @param exporter exporter to export to
   */
  public void export(PatientData.Exporter exporter) {

    exporter.setId(getId());
    exporter.setFacilityCode(getFacilityCode());
    exporter.setLineItems(getLineItems());
    exporter.setPeriodId(getPeriodId());
    exporter.setDataResponse(getDataResponse());
    exporter.setCreatedDate(getCreatedDate());
    exporter.setModifiedDate(getModifiedDate());

  }

  public interface Exporter extends
      BaseTimestampedEntity.BaseTimestampedExporter {

    void setPeriodId(String periodId);

    void setFacilityCode(String facilityCode);

    void setLineItems(List<PatientDataLineItem> lineItems);

    void setDataResponse(DataResponse response);

  }

  public interface Importer extends BaseTimestampedEntity.BaseTimestampedImporter {
    String getPeriodId();

    String getFacilityCode();

    List<PatientDataLineItem> getLineItems();

    DataResponse getDataResponse();

  }

}
