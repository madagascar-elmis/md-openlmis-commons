/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.domain.integration;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * ExportLog Class.
 */
@Entity
@Table(name = "export_log", schema = "common")
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper = true)
@Builder
@Setter
@Getter
public class ExportLog {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "http_status", length = 10)
  private String httpStatus;

  @Column(name = "http_status_code")
  private Integer httpStatusCode;

  @Column(name = "status", length = 10)
  private String status;

  @Column(name = "message", columnDefinition = "TEXT")
  private String message;

  @Column(name = "response_type", length = 50)
  private String responseType;

  @Column(name = "response_status", length = 10)
  private String responseStatus;

  @Column(name = "import_options", columnDefinition = "JSONB")
  private String importOptions;

  @Column(name = "description", columnDefinition = "TEXT")
  private String description;

  @Column(name = "imported")
  private Integer imported;

  @Column(name = "updated")
  private Integer updated;

  @Column(name = "ignored")
  private Integer ignored;

  @Column(name = "deleted")
  private Integer deleted;

  @Column(name = "conflicts", columnDefinition = "JSONB")
  private String conflicts;

  @Column(name = "rejected_indexes", columnDefinition = "JSONB")
  private String rejectedIndexes;

  @Column(name = "data_set_complete")
  private Boolean dataSetComplete;

  @Column(name = "created_at")
  private LocalDateTime createdAt;
}
