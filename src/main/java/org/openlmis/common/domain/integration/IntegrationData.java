/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.domain.integration;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * IntegrationData Class.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "integration_data", schema = "public")
public class IntegrationData {
  @Id
  @Column(name = "id")
  private UUID id;

  @Column(name = "period")
  private Integer period;

  @Column(name = "orgunit")
  private String orgUnit;

  @Column(name = "org_unit_id")
  private String orgUnitId;

  @Column(name = "value")
  private Integer value;

  @Column(name = "dataelement")
  private String dataElement;

  @Column(name = "schedulename")
  private String scheduleName;

  @Column(name = "reporting_year")
  private Integer reportingYear;

  @Column(name = "data_element_id")
  private String dataElementId;

}
