/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.domain;

import java.util.Set;
import java.util.stream.Collectors;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.javers.core.metamodel.annotation.TypeName;

@Entity
@Table(name = "msd_daily_stock_status")
@TypeName("MSDDailyStockStatus")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"ilNumber"}, callSuper = true)
public class MsdDailyStockStatus extends BaseTimestampedEntity {

  @NotBlank
  private String ilNumber;

  @NotEmpty
  @JoinTable(
      name = "msd_daily_stock_status_values",
      joinColumns = @JoinColumn(name = "status_id"),
      inverseJoinColumns = @JoinColumn(name = "value_id")
  )
  @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
  private Set<MsdDailyStockStatusValue> values;

  /**
   * Create new stock status from the provided stock status details.
   *
   * @param importer provided stock status details
   *
   * @return new instance of stock status.
   */
  public static MsdDailyStockStatus newInstance(Importer importer) {
    MsdDailyStockStatus serviceType = new MsdDailyStockStatus();
    serviceType.setId(importer.getId());
    serviceType.setIlNumber(importer.getIlNumber());

    serviceType.setValues(
        importer.getValues()
            .stream()
            .map(value -> MsdDailyStockStatusValue.newInstance(value))
            .collect(Collectors.toSet())
    );

    return serviceType;
  }

  /**
   * Export the content of the stock status entity.
   *
   * @param exporter destination of the exported stock status data.
   */
  public void export(MsdDailyStockStatus.Exporter exporter) {
    exporter.setId(this.id);
    exporter.setValues(this.values);
    exporter.setIlNumber(this.ilNumber);
  }

  public interface Importer extends BaseImporter {

    String getIlNumber();

    Set<? extends MsdDailyStockStatusValue.Importer> getValues();
  }

  public interface Exporter extends BaseExporter {

    void setIlNumber(String ilNumber);

    void setValues(Set<MsdDailyStockStatusValue> values);
  }
}
