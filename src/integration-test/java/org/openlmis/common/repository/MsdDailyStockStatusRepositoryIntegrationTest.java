/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.repository;

import javax.inject.Inject;
import lombok.Getter;
import org.junit.runner.RunWith;
import org.openlmis.common.domain.MsdDailyStockStatus;
import org.openlmis.common.util.MsdDailyStockStatusDataBuilder;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class MsdDailyStockStatusRepositoryIntegrationTest
    extends BaseCrudRepositoryIntegrationTest<MsdDailyStockStatus> {

  @Getter
  @Inject
  private MsdDailyStockStatusRepository repository;

  @Override
  MsdDailyStockStatus generateInstance() throws Exception {
    return new MsdDailyStockStatusDataBuilder().withoutId().build();
  }
}
